var data = [];

var carousel_selected = 2;
var carousel_items = 6;

var positions = [0, 250, 500, 750, 1000, 1250];
var items = ['.carousel-item-1','.carousel-item-2','.carousel-item-3','.carousel-item-4','.carousel-item-5','.carousel-item-6'];

var sliding = false;

var scaleSel = 1;
var scaleNotSel = 0.7;
var scaleSelY = 120;

$( document ).ready(function() {

    initCarouselAnim();

    /*$.getJSON( "data/data.json", function( result ) {

        data = result.carousel;

        for (var i = 0; i < data.length; i++) {
            positions.push(i*250);
            items.push('.carousel-item-'+(i+1));
        }

        Tempo.prepare("carousel").render(result.carousel);

        initCarouselAnim();

        Tempo.prepare("boxes").render(result.boxes);

        resizeCarousel();

        window.addEventListener('resize', function(event){
            resizeCarousel();
        });

    });*/

    resizeCarousel();

        window.addEventListener('resize', function(event){
            resizeCarousel();
        });

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            setXmlData(this);
        }
    };
    xhttp.open("GET", "data/data.xml", true);
    xhttp.send();

    $('body').keydown(function(e) {console.log(e);
	    var code = e.keyCode || e.which;
	    if (code == '9') {
	    	if($(e.target).attr('name') == 'carousel-item'){
	    		if(carousel_tabs < 3){
	    			carousel_tabs++;
	    			tabNext();
    				e.preventDefault();
    				return false;
    			} else {
    				carousel_tabs = 0;
    			}  		
	    	}
	    }
	});
    
});

var carousel_tabs = 0;

function tabNext(){
	adjustScale();

    if(!sliding) {
        sliding = true;

        var tempitem = items[0];
        items.splice(0, 1);
        items.push(tempitem);

        for (var i = 0; i < items.length; i++) {

            if(i == items.length-1){
                TweenMax.fromTo(items[i], 1,{left: 0, alpha: 0.7}, {left: -250, alpha: 0, ease: Power1.easeInOut});
            } else {
                TweenMax.to(items[i], 1, {left: positions[i], ease: Power1.easeInOut});
            }

            if(i == 0) {
                TweenMax.to(items[i], 1, {alpha: 0.7, y: 0, scale: scaleNotSel, ease: Power1.easeInOut});
                //$(items[i]).attr('tabindex', '3');
            }
            if(i == 1){
                carousel_selected = parseInt(items[i].replace('.carousel-item-', ''));
                TweenMax.to(items[i], 1, {alpha: 1, y: scaleSelY, scale: scaleSel, ease: Power1.easeInOut});
                //$(items[i]).attr('tabindex', '4');
            } else {
                if(i == 2){
                    //$(items[i]).attr('tabindex', '5');
                }

                if( i >= 3) {
                    TweenMax.to(items[i], 1, {alpha: 0, y: 0, ease: Power1.easeInOut});
                    //$(items[i]).attr('tabindex', '-1');
                } else {
                    TweenMax.to(items[i], 1, {alpha: 0.7, y: 0, scale: scaleNotSel, ease: Power1.easeInOut});
                }
            }
        }

        setTimeout(function () { sliding = false; } , 1000);

        $(".find-content").fadeOut(1000, function () { onCompleteFindBtn(); });

        $(".company-content").fadeOut('slow', function () {
            onCompleteCompany();
        });
    }
    $('.find-btn').focus();
}

function setXmlData(xml) {
    var xmlDoc = xml.responseXML;

    var carousel = xmlDoc.getElementsByTagName("item");
    var id = 0;
    var href = 1;
    var company = 2;
    var name = 3;
    var alt = 4;
    var tempcompany = '';

    for (var i = 0; i < carousel.length; i++) {
        data.push({'id': carousel[i].childNodes[id].childNodes[0].nodeValue, 'title': carousel[i].childNodes[alt].childNodes[0].nodeValue, 'href': carousel[i].childNodes[href].childNodes[0].nodeValue, 'company': carousel[i].childNodes[company].childNodes[0].nodeValue, 'name': carousel[i].childNodes[name].childNodes[0].nodeValue});
    }
    onCompleteFindBtn();

    var boxes = xmlDoc.getElementsByTagName("box");

    for (var i = 0; i < boxes.length; i++) {
        $("[template='name_"+(i+1)+"']").html(boxes[i].childNodes[3].childNodes[0].nodeValue);//name
        $("[template='alt_"+(i+1)+"']").attr('alt', boxes[i].childNodes[2].childNodes[0].nodeValue);//alt
        $("[template='alt_"+(i+1)+"']").attr('aria-label', boxes[i].childNodes[2].childNodes[0].nodeValue);//title
        $("[template='href_"+(i+1)+"']").attr('href', boxes[i].childNodes[1].childNodes[0].nodeValue);//href
    }

    var title = xmlDoc.getElementsByTagName("title");
    $("[template='title']").html(title[0].childNodes[0].nodeValue);

    var subtitle = xmlDoc.getElementsByTagName("subtitle");
    $("[template='subtitle']").html(subtitle[0].childNodes[0].nodeValue);

}

function adjustScale () {
    if($(window).width() <= 420){
        scaleSel = 1.8;
        scaleNotSel = 1.2;
        scaleSelY = 250;
    } else {
        scaleSel = 1;
        scaleNotSel = 0.7;
        scaleSelY = 120;        
    }    
}

function initCarouselAnim() {

    adjustScale();

    for (var i = 0; i < items.length; i++) {        
        TweenMax.to(items[i], 0.1, {left: positions[i], ease: Power1.easeInOut});

        if(i == 0) {
            TweenMax.fromTo(items[i], 1, {alpha: 0, y: 0, scale: 0}, {alpha: 0.7, y: 0, scale: scaleNotSel, ease: Power1.easeInOut});
        }
        if(i == 1){
            TweenMax.fromTo(items[i], 1, {alpha: 0, y: scaleSelY, scale: 0}, {alpha: 1, y: scaleSelY, scale: scaleSel, ease: Power1.easeInOut});
        } else {
            if( i >= 3) {
                TweenMax.to(items[i], 0.1, {alpha: 0, y: 0, ease: Power1.easeInOut});
            } else {
                TweenMax.fromTo(items[i], 1, {alpha: 0, y: 0, scale: 0}, {alpha: 0.7, y: 0, scale: scaleNotSel, ease: Power1.easeInOut});
            }
        }
    }
    
    $( ".f-next" ).click(function() {

        adjustScale();

        if(!sliding) {
            sliding = true;

            var tempitem = items[0];
            items.splice(0, 1);
            items.push(tempitem);

            for (var i = 0; i < items.length; i++) {

                if(i == items.length-1){
                    TweenMax.fromTo(items[i], 1,{left: 0, alpha: 0.7}, {left: -250, alpha: 0, ease: Power1.easeInOut});
                } else {
                    TweenMax.to(items[i], 1, {left: positions[i], ease: Power1.easeInOut});
                }

                if(i == 0) {
                    TweenMax.to(items[i], 1, {alpha: 0.7, y: 0, scale: scaleNotSel, ease: Power1.easeInOut});
                    //$(items[i]).attr('tabindex', '3');
                }
                if(i == 1){
                    carousel_selected = parseInt(items[i].replace('.carousel-item-', ''));
                    TweenMax.to(items[i], 1, {alpha: 1, y: scaleSelY, scale: scaleSel, ease: Power1.easeInOut});
                    //$(items[i]).attr('tabindex', '4');
                } else {
                    if(i == 2){
                        //$(items[i]).attr('tabindex', '5');
                    }

                    if( i >= 3) {
                        TweenMax.to(items[i], 1, {alpha: 0, y: 0, ease: Power1.easeInOut});
                        //$(items[i]).attr('tabindex', '-1');
                    } else {
                        TweenMax.to(items[i], 1, {alpha: 0.7, y: 0, scale: scaleNotSel, ease: Power1.easeInOut});
                    }
                }
            }

            setTimeout(function () { sliding = false; } , 1000);

            $(".find-content").fadeOut(1000, function () { onCompleteFindBtn(); });

            $(".company-content").fadeOut('slow', function () {
                onCompleteCompany();
            });
        }

    });

    $( ".f-previous" ).click(function() {

        adjustScale();

        if(!sliding) {
            sliding = true;

            var tempitem = items[items.length-1];
            items.splice(-1, 1);
            items.splice(0, 0, tempitem);

            for (var i = 0; i < items.length; i++) {

                if(i == 0){
                    TweenMax.fromTo(items[i], 1,{left: -250, alpha: 0}, {left: positions[i], alpha: 0.7, ease: Power1.easeInOut});
                    //$(items[i]).attr('tabindex', '3');
                } else {
                    TweenMax.to(items[i], 1, {left: positions[i], ease: Power1.easeInOut});
                }

                if(i == 1){
                    TweenMax.to(items[i], 1, {alpha: 1, y: scaleSelY, scale: scaleSel, ease: Power1.easeInOut});
                    carousel_selected = parseInt(items[i].replace('.carousel-item-', ''));
                    //$(items[i]).attr('tabindex', '4');
                } else {

                    if(i == 2){
                        //$(items[i]).attr('tabindex', '5');
                    }

                    if( i >= 3) {
                        TweenMax.to(items[i], 1, {alpha: 0, y: 0, ease: Power1.easeInOut});
                        //$(items[i]).attr('tabindex', '-1');
                    } else {
                        TweenMax.to(items[i], 1, {alpha: 0.7, y: 0, scale: scaleNotSel, ease: Power1.easeInOut});                        
                    }
                }
            }

            setTimeout(function () { sliding = false; } , 1000);

            $(".find-content").fadeOut(1000, function () { onCompleteFindBtn(); });            

            $(".company-content").fadeOut('slow', function () {
                onCompleteCompany();
            });
        }

    });
}

function onCompleteFindBtn() {
    
    $(".find-btn span").html('FIND YOUR...');
    $(".find-btn").attr('href', '#');

    for (var i = 0; i < data.length; i++) {
        if(parseInt(data[i].id) == carousel_selected){
            $(".find-btn span").html('FIND YOUR '+data[i].name);
            $(".find-btn").attr('href', data[i].href);
            $(".find-btn").attr('aria-label', data[i].title);
            $(".carousel-item-"+carousel_selected).attr('href', data[i].href);
        }
        $(".carousel-item-"+data[i].id).attr('aria-label', data[i].title);
    }

    $(".find-content").fadeIn(1000);
}

function onCompleteCompany() {

    $(".company-content .title").html('MAGIC');

    for (var i = 0; i < data.length; i++) {
        if(parseInt(data[i].id) == carousel_selected){
            $(".company-content .title").html(data[i].company);
        }
    }
    $(".company-content").fadeIn();
}

function resizeCarousel(){
    var z = 1;
    if($(window).width() <= 1085){
        z = $(window).width()/1085;
    }    
    $('#carousel').css('zoom', z.toFixed(4));
}

function dialogGameHide(){
    $('#dialog-game').hide();
}
function dialogGameShow(){    
    $('#dialog-game').show();
}